import React from 'react';

export const Store = React.createContext();

const initialState = {
  user: {},
  uploaded_files: [],
  breadcrumbs: [],
  shorten_url: '',
  accounts: [],
  loading: {
    current: true
  },
  current_page: 1
};

function reducer(state, action) {
  switch (action.type) {
    case 'STORE_UPLOADED_FILE':
      const { files, folders } = action.payload;
      files.length && files.sort((a, b) => b.last_modified.localeCompare(a.last_modified));
      folders.length && folders.sort((a, b) => b.name.localeCompare(a.name));
      return { ...state, uploaded_files: [...folders, ...files] };
    case 'SET_BREADCRUMBS':
      return { ...state, breadcrumbs: [...action.payload] };
    case 'STORE_USERNAME':
      return {
        ...state, user: { ...state.user, username: action.payload }
      };
    case 'REMOVE_FILE':
      return {
        ...state,
        uploaded_files: state.uploaded_files.filter(file => action.payload.type === 'folder' ? file.name !== action.payload.name : file.key !== action.payload.key),
      };
    case 'STORE_SHORTEN_URL':
      return { ...state, shorten_url: action.payload }
    case 'CLEAR_SHORTEN_URL':
      return { ...state, shorten_url: '' }
    case 'STORE_ACCOUNTS':
      return { ...state, accounts: action.payload }
    case 'SET_LOADING':
      return { ...state, loading: action.payload }
    case 'SET_CURRENT_PAGE':
      return { ...state, current_page: action.payload }
    default:
      return state;
  }
}

export function StoreProvider(props) {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  const value = { state, dispatch };
  return <Store.Provider value={value}>{props.children}</Store.Provider>;
}