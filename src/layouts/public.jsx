import React, { Fragment } from 'react';

export default function PublicLayout(props) {
  return (
    <Fragment>
      <main className="container">
        { props.children }
      </main>
    </Fragment>
  )
}