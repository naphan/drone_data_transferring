import React, { Fragment } from 'react';
import Header from './../commons/header';
import Footer from './../commons/footer';

export default function PrivateLayout(props) {
  return (
    <Fragment>
      <Header {...props}/>
      <main className="container">
        { props.children }
      </main>
      <Footer/>
    </Fragment>
  )
}