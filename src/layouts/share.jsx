import React, { Fragment } from 'react';
import Footer from './../commons/footer';

export default function ShareLayout(props) {
  return (
    <Fragment>
      <main className="share-container">
        { props.children }
      </main>
      <Footer/>
    </Fragment>
  )
}