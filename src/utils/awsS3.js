import AWS from 'aws-sdk';

AWS.config.region = process.env.REGION;
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: process.env.IDENTIFY_POOL_ID
})

const s3 = new AWS.S3({
  apiVersion: '2006-03-01',
  useAccelerateEndpoint: true,
  params: {
    Bucket: process.env.BUCKET_NAME
  }
})

const awsS3 = {
  upload(albumName, file, config) {
    return new Promise((resolve, reject) => {
      const params = {
        AccelerateConfiguration: { /* required */
          Status: 'Enabled'
        },
      }

      s3.putBucketAccelerateConfiguration(params, (err, data) => {
        if (err) {
          return reject(new Error(err))
        }
        if (data) {
          let albumKey = awsS3.encodeAlbum(albumName);
          albumKey = `${albumKey.join('/')}/${file.name}`;
          const params = {
            Key: `${albumKey}`,
            Body: file
          }

          const session = s3.upload(params, (err, data) => {
            if (err) {
              return reject(new Error(err))
            }
            if (data) {
              return resolve({ imgUrl: data.Location })
            }
          })
            .on('httpUploadProgress', progress => config.httpUploadProgress(session, progress))
        }
      })
    })
  },

  cancel(session) {
    if (Object.entries(session).length !== 0 && session.constructor === Object) {
      try {
        session.abort()
      } catch (e) {
        //
      }
    }
  },

  deleteFile(key) {
    const params = {
      Key: key,
    }

    return new Promise((resolve, reject) => {
      s3.deleteObject(params, (err) => {
        if (err) {
          return reject(new Error(err));
        }
        return resolve({message: 'Delete success'})
      })
    })
  },

  emptyBucket(albumName){
    const albumKey = awsS3.encodeAlbum(albumName);
    let params = {
      Prefix: `${albumKey.join('/')}/`,
      Delimiter: '/'
    }

    return new Promise((resolve, reject) => {
      s3.listObjects(params, (err, data) => {
        if (err) {
          reject(new Error(err.message));
        }
        params = { Delete: { Objects: [] } };
        data.Contents.forEach(content => params.Delete.Objects.push({ Key: content.Key }));

        s3.deleteObjects(params, (err) => {
          if (err) {
            reject(new Error(err.message));
          }

          if (data.isTruncated) {
            awsS3.emptyBucket(albumName)
          }
          resolve('success')
        })
      });
    })
  },

  listFiles(prefix) {
    const params = {
      Prefix: prefix,
    }

    return new Promise((resolve, reject) => {
      const files = [];

      s3.listObjects(params)
        .on('error', function (err) {
          reject(new Error(err))
        })
        .on('success', function handlePage(response) {
          const href = response.request.httpRequest.endpoint.href;
          const bucketUrl = `${href}${process.env.BUCKET_NAME}/`;
          const data = response.data.Contents.map(file => {
            return {
              id: file.ETag,
              name: file.Key.split('/').pop(),
              author: file.Key.split('/').shift(),
              size: file.Size,
              url: `${bucketUrl}${file.Key}`,
              key: file.Key,
              last_modified: new Date(file.LastModified).toLocaleString()
            }
          })
          files.push(...data);
          if (response.hasNextPage()) {
            response.nextPage().on('success', handlePage).send();
          } else {
            resolve(files);
          }
        }).send();
    })
  },

  createFolder(name, albumName) {
    return new Promise((resolve, reject) => {
      name = name.trim();
      if (!name) {
        reject(new Error('Folder name must contain at least one non-space character.'));
      }
      if (name.indexOf('/') !== -1) {
        reject(new Error('Folder name cannot contain slashes.'));
      }
      let albumKey = awsS3.encodeAlbum(albumName);
      const nameKey = `${albumKey.join('/')}/${encodeURIComponent(name)}/`;

      s3.headObject({Key: nameKey}, function (err, data) {
        if (!err) {
          reject(new Error('Folder already exists.'));
        }
        if (err && err.code !== 'NotFound') {
          return (new Error(err.message));
        }

        s3.putObject({Key: nameKey}, function (err, data) {
          if (err) {
            console.log(err);
            reject(new Error(err.message));
          }
          resolve(awsS3.listBucket(albumName))
        });
      });
    })
  },

  listBucket(albumName) {
    let nameKey = awsS3.encodeAlbum(albumName)
    nameKey = `${nameKey.join('/')}/`;
    return new Promise((resolve, reject) => {
      const files = [];
      s3.listObjects({ Prefix: nameKey, Delimiter: '/'})
        .on('error', function (err) {
          reject(new Error(err))
        })
        .on('success', function handlePage(response) {
          const href = response.request.httpRequest.endpoint.href;
          const bucketUrl = `${href}${process.env.BUCKET_NAME}/`;
          const folders = response.data.CommonPrefixes.map(commonPrefix => {
            const names = commonPrefix.Prefix.split('/').map((prefix, index) => {
              if (index === 0) {
                return prefix;
              }
              return decodeURIComponent(prefix);
            })
            return {
              type: 'folder',
              name: names.filter(Boolean).pop()
            }
          });
          const data = response.data.Contents.map(file => {
            return {
              type: 'file',
              id: file.ETag,
              name: file.Key.split('/').pop(),
              author: file.Key.split('/').shift(),
              size: file.Size,
              url: `${bucketUrl}${encodeURIComponent(file.Key)}`,
              key: file.Key,
              last_modified: new Date(file.LastModified).toLocaleString()
            }
          })
          files.push(...data.filter(file => file.size));
          if (response.hasNextPage()) {
            response.nextPage().on('success', handlePage).send();
          } else {
            resolve({files, folders});
          }
        }).send();
    })
  },

  encodeAlbum(albumName = []) {
    return albumName.map((album, index) => {
      if (index === 0) {
        return album;
      }

      return encodeURIComponent(album);
    })
  }
}

export default awsS3;

