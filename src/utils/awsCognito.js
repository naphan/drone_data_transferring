import AWS from 'aws-sdk';
import { AuthenticationDetails, CognitoUserPool, CognitoUser, CognitoIdentityCredentials } from 'amazon-cognito-identity-js';
import { navigate } from "@reach/router"

const awsCognito = {
  getUserPool () {
    const poolData = {
      UserPoolId : process.env.POOL_ID, // Your user pool id here
      ClientId : process.env.CLIENT_ID // Your client id here
    };

    return new CognitoUserPool(poolData);
  },

  authentication(authData) {
    const authDetail = new AuthenticationDetails(authData);

    const userPool = this.getUserPool();
    const userData = {
      Username: authData.Username,
      Pool: userPool,
    };
    const cognitoUser = new CognitoUser(userData);
    return new Promise((resolve, reject) => {
      cognitoUser.authenticateUser(authDetail, {
        onSuccess: (result) => {
          const accessToken =  result.getAccessToken().getJwtToken();
          const idToken = result.idToken.jwtToken;
          resolve({ accessToken, idToken });
        },
        onFailure: (err) => {
          reject(new Error(err.message));
        }
      })
    })
  },

  getCurrentUser() {
    const userPool = this.getUserPool();

    return new Promise((resolve, reject) => {
      const cognitoUser = userPool.getCurrentUser();
      if (cognitoUser != null) {
        cognitoUser.getSession(function(err, session) {
          if (err) {
            return reject(new Error(err))
          }
          AWS.config.region = process.env.REGION
          AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId : process.env.IDENTIFY_POOL_ID, // your identity pool id here
            Logins : {
              // Change the key below according to the specific region your user pool is in.
              [`cognito-idp.${process.env.REGION}.amazonaws.com/${process.env.POOL_ID}`] : session.getIdToken().getJwtToken()
            }
          });
          // call refresh method in order to authenticate user and get new temp credentials
          if (!session.isValid()) {
            AWS.config.credentials.refresh((error) => {
              if (error) {
                return reject(new Error(err))
              } else {
                return resolve(session);
              }
            })
          }
          return resolve(session)
        })
      } else {
        return reject('You are not login')
      }
    })
  },
  signOut() {
    const data = {
      UserPoolId : process.env.POOL_ID, // Your user pool id here
      ClientId : process.env.CLIENT_ID // Your client id here
    };
    const userPool = new CognitoUserPool(data);
    const cognitoUser = userPool.getCurrentUser();
    if (cognitoUser != null) {
      cognitoUser.signOut()
    }
    navigate('/login')
  }
}

export default awsCognito;
