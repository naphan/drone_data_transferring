import toastr from 'toastr'
import 'toastr/toastr.scss'

class NotificationHelper {
  constructor() {
    this.toastr = toastr
    this.toastr.options = {
      closeButton: true,
      debug: false,
      newestOnTop: false,
      progressBar: true,
      preventDuplicates: true,
      preventOpenDuplicates: true,
      updateTimerOnDuplicates: true,
      onclick: null,
      hideDuration: 0,
      showDuration: 300,
      timeOut: 5000,
      extendedTimeOut: 1000,
      showEasing: 'swing',
      hideEasing: 'linear',
      showMethod: 'fadeIn',
      hideMethod: 'fadeOut',
      tapToDismiss: false,
      positionClass: 'toast-bottom-right'
    }
  }

  success(message) {
    this.toastr.success(message)
  }

  error(message) {
    this.toastr.error(message)
  }

  warning(message) {
    this.toastr.error(message)
  }

  // @TODO: Comment waiting for using data variable
  show(type, message) {
    switch (type) {
      case 'error':
        this.toastr.error(message)
        break
      case 'success':
        this.toastr.success(message)
        break
      case 'warning':
        this.toastr.warning(message)
        break
      default:
        this.toastr.info(message)
        break
    }
  }
}

export default NotificationHelper
