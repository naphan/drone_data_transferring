'use strict'
const url = require('url');
const AWS = require('aws-sdk');

const s3 = new AWS.S3();
const config = require('./config.json')

console.info('Function is processing!',url)
exports.handler = (event, context, callback) => {
  let longUrl = JSON.parse(event.body).url || '';

  validate(longUrl)
    .then(() => getPath())
    .then(path => {
      const redirect = buildRedirect(path, longUrl)
      return saveRedirect(redirect);
    })
    .then(path => {
      const response = buildResponse(200, 'URL successfully shortened', path);
      return Promise.resolve(response);
    })
    .catch(err => {
      console.error('err', err)
      const response = buildResponse(err.statusCode, err.message)
      return Promise.resolve(response);
    })
    .then(response => callback(null, response))
}

function validate(longUrl) {
  if (longUrl === '') {
    return Promise.reject({
      statusCode: 400,
      message: 'URL is required'
    })
  }

  let parsedUrl = url.parse(longUrl);
  if (parsedUrl.protocol === null || parsedUrl.host === null) {
    return Promise.reject({
      statusCode: 400,
      message: 'URL is invalid'
    });
  }

  return Promise.resolve(longUrl);
}

function getPath() {
  return new Promise((resolve, reject) => {
    const path = generatePath();
    isPathFree(path)
      .then(isFree => {
        return isFree ? resolve(path) : resolve(getPath());
      })
      .catch(err => {
        console.log('an error has occured', err.message)
      })
  })
}

function generatePath(path = '') {
  const characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  const position = Math.floor(Math.random() * characters.length);
  const character = characters.charAt(position);

  if (path.length === 7) {
    return path;
  }

  return generatePath(path + character);
}

function isPathFree(path) {
  return s3.headObject(buildRedirect(path)).promise()
    .then(() => Promise.resolve(false))
    .catch(err => {
      return err.code === 'NotFound' ? Promise.resolve(true) : Promise.reject(err)

    })
}

function saveRedirect(redirect) {
  return s3.putObject(redirect).promise()
    .then(() => Promise.resolve(redirect['Key']))
}

function buildRedirect(path, longUrl = false) {
  const redirect = {
    'Bucket': config.BUCKET,
    'Key': path
  }

  if (longUrl) {
    redirect['WebsiteRedirectLocation'] = longUrl;
  }

  return redirect;
}

function buildRedirectUrl(path) {
  let baseUrl = 'https://s3-transfer.terra-drone.net';

  if ('BASE_URL' in config && config['BASE_URL'] !== '') {
    baseUrl = config['BASE_URL'];
  }

  return `${baseUrl}/${path}`;
}

function buildResponse(statusCode, message, path = false) {
  const body = { message };

  if (path) {
    body['path'] = path;
    body['url'] = buildRedirectUrl(path);
  }

  return {
    'headers': {
      'Access-Control-Allow-Origin': '*'
    },
    statusCode,
    body: JSON.stringify(body)
  }
}
