import "@babel/polyfill";
import React, {Fragment} from 'react';
import ShareLayout from './layouts/share';

export default function Public(props) {
  return (
    <Fragment>
      <ShareLayout>{ props.children }</ShareLayout>
    </Fragment>
  )
}