import "@babel/polyfill";
import { navigate } from '@reach/router';
import React, { useContext, useEffect, useRef, useState } from 'react';

import awsCognito from "./utils/awsCognito";

import { Store } from './Store';
import { storeUserName, setBreadcrumbs, storeAccounts, setActiveAccount } from "./pages/Login/action";

import PublicLayout from './layouts/public'
import PrivateLayout from './layouts/private'
import awsS3 from "./utils/awsS3";
import {setCurrentPage, setLoading, storeUploadedFiles} from "./pages/Home/action";

function App(props) {
  const authenticated = useRef(false);
  const { state, dispatch } = useContext(Store);
  const [profileShow, setProfileShow] = useState(false);

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const uname_param = urlParams.get('username');
    uname_param && storeAccounts(uname_param, 'guest', true)
    authenticated.current = true;
    awsCognito.getCurrentUser()
      .then(result => {
        const { username } = result.getAccessToken().payload;
        const accounts = JSON.parse(localStorage.getItem('accounts'));
        storeAccounts(username, 'owner');
        setBreadcrumbs([accounts.find(account => account.status).name], dispatch);
        storeUserName(username, dispatch);
        navigate('/');
      })
      .catch(() => {
        authenticated.current = false;
        navigate('/login');
      })

    return () => {
      authenticated.current = false;
    }

  }, [])

  const handleSignOut = () => {
    awsCognito.signOut()
    authenticated.current = false;
    setBreadcrumbs([], dispatch);
    setProfileShow(false);
    localStorage.setItem('breadcrumbs', JSON.stringify([]));
    localStorage.setItem('accounts', JSON.stringify([]));
  }

  const handleProfileShow = (bool) => {
    setProfileShow(!bool)
  }

  const handleSelectAccount = username => {
    setBreadcrumbs([username], dispatch);
    localStorage.setItem('breadcrumbs', JSON.stringify([username]));
    setActiveAccount(username);
    setProfileShow(false);
    setCurrentPage(1, dispatch);
    storeUploadedFiles({files: [], folders: []}, dispatch);
    setLoading(true, dispatch);
    awsS3.listBucket([username])
      .then(data => {
        setLoading(false, dispatch);
        storeUploadedFiles(data, dispatch);
      })
      .catch(err => {
        setLoading(false, dispatch);
        // toastr.error("An error occurred!")
      })
  }

  const subProps = {
    profileShow,
    handleSignOut,
    user: state.user,
    handleProfileShow,
    handleSelectAccount,
    accounts: JSON.parse(localStorage.getItem('accounts')),
  }

  return (
    <React.Fragment>
      { authenticated.current || props.location.pathname !== '/login'
        ? <PrivateLayout {...subProps}>{props.children}</PrivateLayout>
        : <PublicLayout>{props.children}</PublicLayout>
      }
    </React.Fragment>
  );
}
export default App;