import React from 'react';

export default function Footer() {
  return (
    <footer className="footer">
      <p>Terra Drone &copy; 2019</p>
    </footer>
  )
}