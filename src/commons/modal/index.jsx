import React from 'react'
import cl from "classnames";

export default function ShareModal(props) {

  const {
    shorten_url,
    collapsedModal,
    handleOpenShareModal
  } = props

  return (
    <section className={cl('modal-share', {collapsedModal: collapsedModal})}>
      <div className="bg-transparent"></div>
      <div className="modal-contain">
        <div className="modal-header">
          <div className="header-title">
            <span className="title-icon"><i className="fas fa-share-alt"></i></span>
            <span className="title-text">Share S3 bucket</span>
          </div>
          <i className="fas fa-times" onClick={() => handleOpenShareModal(false)}></i>
        </div>
        <div className="modal-body">
          <span className="label-url">Generate URL:</span>
          <span className="input-text">{ shorten_url }</span>
        </div>
      </div>
    </section>
  )
}