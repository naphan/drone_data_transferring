import React from 'react';

export default function Header(props) {
  const { accounts, profileShow, handleProfileShow, handleSignOut, handleSelectAccount } = props;
  const activeAccount = accounts && accounts.length && accounts.find(account => account.status);
  const deactiveAccount = accounts && accounts.length && accounts.filter(account => !account.status);
  return (
    <header className="navigation">
      <div className="left-group">
        <button
          type="button"
          className="profile"
          onClick={() => handleProfileShow(profileShow)}
          title={accounts && activeAccount.name}
        >
          { accounts && accounts.length && activeAccount.name.charAt(0).toUpperCase() }
        </button>
        { profileShow && <div className="dropdown">
          <ul className="menues">
            {
              accounts && <li className="menu-item">
                <button type="button">
                  <div className="avatar">
                    { accounts && accounts.length && activeAccount.name.charAt(0).toUpperCase() }
                  </div>
                  <div className="account-detail">{ accounts && activeAccount.name }</div>
                </button>
              </li>
            }
            {
              accounts && accounts.length && deactiveAccount.map((account, index) => (
                  <li className="menu-item" key={index} title={account.name}>
                    <button className="button" onClick={() => handleSelectAccount(account.name)}>
                      <div className="avatar">
                        { account.name.charAt(0).toUpperCase() }
                      </div>
                      <div className="account-detail">{ account.name }</div>
                    </button>
                  </li>
                ))
            }
            <li className="menu-item">
              <button type="button" onClick={handleSignOut}>
                <i className="icon-logout"></i> Sign out
              </button>
            </li>
          </ul>
        </div> }
      </div>
    </header>
  )
}