export const storeUserName = (data, dispatch) => {
  let dispatchObject = {
    type: 'STORE_USERNAME',
    payload: data
  }
  return dispatch(dispatchObject)
}

export const storeAccounts = (username, role, active = false) => {
  const accounts = JSON.parse(localStorage.getItem('accounts'));

  if (accounts && accounts.length > 0) {
    if (accounts.filter(account => account.name === username).length === 0) {
      active && accounts.map(account => account.status = false);
      accounts.push({
        name: username,
        role,
        status: active
      });
      localStorage.setItem('accounts', JSON.stringify(accounts))
    }
  } else {
    localStorage.setItem('accounts', JSON.stringify([{
      name: username,
      role,
      status: true
    }]))
  }
}

export const setActiveAccount = (username) => {
  const accounts = JSON.parse(localStorage.getItem('accounts'));
  accounts.map(account => {
    if (account.status === true) {
      account.status = false;
    }

    if (account.name === username) {
      return account.status = true;
    }

    return account;
  })

  localStorage.setItem('accounts', JSON.stringify(accounts))
}

export const setBreadcrumbs = (data, dispatch) => {
  return dispatch({
    type: 'SET_BREADCRUMBS',
    payload: data
  })
}