import cl from 'classnames';
import { navigate } from "@reach/router"
import React, { useEffect, useState, useContext } from 'react';

import { Store } from './../../Store';
import awsCognito from '../../utils/awsCognito';
import {setBreadcrumbs, storeAccounts, storeUserName} from './action'

import './style.scss'
import logoLogin from '../../assets/images/logo.jpg'

export default function LoginPage() {
  const { dispatch } = useContext(Store);
  const [message, setMessage]   = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [disabled, setDisabled] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault()
    setDisabled(true);
    const authData = {
      Username : username,
      Password : password
    }
    awsCognito.authentication(authData)
      .then(() => {
        setDisabled(false);
        awsCognito.getCurrentUser()
          .then(result => {
            const { payload } = result.accessToken
            storeAccounts(payload.username, 'owner');
            storeUserName(payload.username, dispatch);
            const accounts = JSON.parse(localStorage.getItem('accounts'));
            setBreadcrumbs([accounts.find(account => account.status).name], dispatch);
            navigate('/')
          })
          .catch(error => {
            navigate('/login')
          })
      })
      .catch(error => {
        setDisabled(false);
        setMessage(error.message)
      })
  }

  return (
    <div className="login-container">
      <div className="login-page">
        <form className="login-form" encType="multipart/form-data" onSubmit={handleSubmit}>
          <div className="body-form">
            <img src={logoLogin}/>
            <div className="form-block">
              <input type="text"
                     placeholder="User Name"
                     value={username}
                     onChange={e => setUsername(e.target.value)}/>
            </div>
            {
              message && <div className="help-text">
                { message }
              </div>
            }
            <div className="form-block">
              <input type="password"
                     placeholder="Password"
                     value={password}
                     onChange={e => setPassword(e.target.value)}/>
            </div>
          </div>
          <div className="footer-form">
            <button type="submit" disabled={!username | !password}>
              { disabled && <i className="icon-spin2 animate-spin"></i> }<span>Login</span>
            </button>
          </div>
          </form>
      </div>
    </div>
  )
}