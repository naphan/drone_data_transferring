export const removeFile = (data, dispatch) => {
  let dispatchObject = {
    type: 'REMOVE_FILE',
    payload: data
  }
  return dispatch(dispatchObject);
}

export const storeUploadedFiles = (data, dispatch) => {
  return dispatch({
    type: 'STORE_UPLOADED_FILE',
    payload: data,
  })
}

export const generateShortLink = async (apiUrl, longUrl, dispatch) => {
  const data = await fetch(apiUrl, {
    method: 'POST',
    body: JSON.stringify({ url: longUrl }),
    headers: {
      'Content-type': 'application/json'
    }
  })
  const dataJson = await data.json();
  const path = dataJson.path.replace('short_code', '')
  return dispatch({
    type: 'STORE_SHORTEN_URL',
    payload: dataJson.url
  })
}

export const clearShortLink = (dispatch) => {
  return dispatch({
    type: 'CLEAR_SHORTEN_URL'
  })
}

export const setLoading = (bool, dispatch) => {
  return dispatch({
    type: 'SET_LOADING',
    payload: {current: bool}
  })
}

export const setCurrentPage = (number, dispatch) => {
  return dispatch({
    type: 'SET_CURRENT_PAGE',
    payload: number
  })
}