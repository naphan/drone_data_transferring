import cl from 'classnames';
import { Store } from './../../Store';
import awsS3 from './../../utils/awsS3';
import encrpter from "../../utils/encrypter";
import React, { useContext, useRef, useState, useEffect } from 'react';

import { removeFile, storeUploadedFiles, generateShortLink, clearShortLink, setLoading, setCurrentPage } from "./action";
import { setBreadcrumbs } from "../Login/action";
import Toastr from './../../utils/notifications'
import './style.scss';
import ShareModal from '../../commons/modal'
import awsCognito from "../../utils/awsCognito";

const FileList = React.lazy(() => import('./components/FileList'));
const toastr = new Toastr()

export default function HomePage() {
  const inputEl = useRef(null);
  // const loading = useRef(true);
  let [queued, setQueued]                   = useState(0);
  const [queuing, setQueueing]              = useState(0);
  // const [currentPage, setCurrentPage]       = useState(1);
  const [files, setFiles]                   = useState([]);
  const [uploaded, setUploaded]             = useState([]);
  const [folderName, setFolderName]         = useState('');
  const [errMessage, setErrMessage]         = useState('');
  const [hideForm, setHideForm]             = useState(false);
  const [collapsed, setCollapsed]           = useState(false);
  const { state, dispatch }                 = useContext(Store);
  const [creatingFolder, setCreatingFolder] = useState(false);
  const [collapsedModal, setCollapsedModal] = useState(false)

  const { breadcrumbs, user, uploaded_files, shorten_url, loading, current_page } = state
  const local = JSON.parse(localStorage.getItem('breadcrumbs'));
  const accounts = JSON.parse(localStorage.getItem('accounts'));
  const activeUser = accounts && accounts.length && accounts.find(account => account.status);

  useEffect(() => {
    // loading.current = true;
    setLoading(true, dispatch);
    if (accounts && accounts.length && !accounts[0].role) {
      awsCognito.signOut();
      setBreadcrumbs([], dispatch);
      localStorage.setItem('breadcrumbs', JSON.stringify([]));
      localStorage.setItem('accounts', JSON.stringify([]));
    }

    user && user.username && awsS3.listBucket((local && local.length) ? local : breadcrumbs)
      .then(data => {
        // loading.current = false;
        setLoading(false, dispatch);
        storeUploadedFiles(data, dispatch);
        local && local.length && setBreadcrumbs(local, dispatch)
      })
      .catch(() => {
        toastr.error('An error has occurred!');
      })

    return () => {
      storeUploadedFiles({files: [], folders: []}, dispatch);
      clearShortLink(dispatch);
      // loading.current = true;
      setLoading(true, dispatch)
    };
  }, [user])

  const handleFileChange = () => {
    const inputFiles = Object.values(inputEl.current.files);
    const data = _analyseFiles(inputFiles);
    const queue = process.env.QUEUE_SIZE;
    setFiles(data);

    if (data.length > queue) {
      setQueueing(queue);
    } else {
      setQueueing(data.length)
    }
  }

  const _analyseFiles = inputFiles => {
    const output = inputFiles.map(file => {
      file.id = Math.random().toString(16).slice(2);
      file.xhr = {};
      file.progress = {};
      return file
    })

    return output;
  }

  const handleUpload = (e) => {
    e.preventDefault();
    setHideForm(true);
    const queue = process.env.QUEUE_SIZE;

    if (files.length > queue) {
      for (let i = 0; i < queue; i++) {
        _uploadExecution(files[i]);
      }
    } else {
      files.forEach(file => _uploadExecution(file));
    }
  }

  const _uploadExecution = (file) => {
    const queue = Number(process.env.QUEUE_SIZE);
    const config = {
      httpUploadProgress: (xhr, progress) => _updateProgress(xhr, progress, file)
    }

    awsS3.upload(breadcrumbs, file, config)
      .then(data => {
        queued++;
        setQueued(queued);
        if (queued === queue && queuing < files.length) {
          _upNext();
        }
      })
      .catch((err) => {
        setQueued(0);
        if (files.length > (queuing + queue)) {
          setQueueing(queuing + queue);

          for (let i = queuing; i < queuing + queue; i++) {
            _uploadExecution(files[i]);
          }
        } else {
          setQueueing(files.length)

          for (let i = queuing; i < files.length; i++) {
            _uploadExecution(files[i]);
          }
        }
      })
  }

  const _updateProgress = (xhr, progress, item) => {
    if (progress.loaded === progress.total) {
      setUploaded(uploaded => [...uploaded, item]);
    }
    setFiles(files.map(file => {
      if (file.id === item.id) {
        return Object.assign(file, {
          xhr,
          progress
        })

      }
      return file
    }))
  }

  const _upNext = () => {
    const queue = process.env.QUEUE_SIZE;
    setQueued(0);
    if (files.length > (queuing + queue)) {
      setQueueing(queuing + queue);

      for (let i = queuing; i < queuing + queue; i++) {
        _uploadExecution(files[i]);
      }
    } else {
      setQueueing(files.length)

      for (let i = queuing; i < files.length; i++) {
        _uploadExecution(files[i]);
      }
    }
  }

  const _generateDate = () => {
    const date = new Date();
    const y = date.getFullYear();
    const m = date.getMonth() + 1;
    const d = date.getDate();
    return `${y}-${m}-${d}`;
  }

  const showProgressBars = () => {
    if (uploaded.length === files.length) {
      setHideForm(false);
      inputEl.current.value = '';
      setUploaded([]);
      setFiles([]);
      // loading.current = true
      setLoading(true, dispatch);
      setTimeout(() => awsS3.listBucket(breadcrumbs)
        .then(data => {
          storeUploadedFiles(data, dispatch);
          toastr.success("Upload succeed!")
          // loading.current = false;
          setLoading(false, dispatch);
        })
        .catch(err => {
          toastr.error("An error has occurred!")
          // loading.current = false;
          setLoading(false, dispatch);
        }), 2500);
    }
    return files.map((file, key) => {
      let uploadedSize = 0;
      const percentage = Math.floor((file.progress.loaded * 100) / file.progress.total);

      if (Object.entries(file.progress).length !== 0 && file.progress.constructor === Object) {
        uploadedSize = (file.progress.loaded / (1000 ** 2)).toFixed(1)
      }

      return (
        <div className="progress-bar-item" key={key}>
          <div className="item-left">
            <i className="material-icons"></i>
          </div>
          <div className="item-center">
            <label
              htmlFor="image">
              <span className="filename">{file.name}</span>
              <span className="filesize">{`${uploadedSize}/${(file.size / (1000 ** 2)).toFixed(1)}MB`}</span>
            </label>
            <div className="linear-progress-container">
              <div className="linear-progress" style={{ height: '5px', backgroundColor: '#0076E8', width: percentage ? `${percentage}%` : 0 }}></div>
            </div>
            {/*{ !image.status ? <div className="upload-error">{ intl.formatMessage(messages.uploadError) }</div> : '' }*/}
          </div>
          <div className="item-right">
            { percentage === 100 && <i className="icon-ok-circled2-1"></i> }
            {/*{ errorImage.length > 0 && <i className="error material-icons" onClick={() => this.handleRefresh(image)}>refresh</i> }*/}
          </div>
        </div>
      )
    })
  }

  const handleDelete = (file) => {
    loading.current = true;
    if (file.type === 'folder') {
      awsS3.emptyBucket([...breadcrumbs, file.name])
        .then(() => {
          loading.current = false;
        _arrangePageAfterDelete(file);
          toastr.success('Folder has been deleted successfully');
      })
        .catch(err => {
          loading.current = false;
          toastr.error('An error has occurred!');
        })
    } else {
      awsS3.deleteFile(file.key)
        .then(() => {
          loading.current = false;
          _arrangePageAfterDelete(file);
          toastr.success('File has been deleted successfully');
        })
        .catch((err) => {
          loading.current = false;
          toastr.error('An error has occurred!');
        })
    }
  }

  const _arrangePageAfterDelete = (file) => {
    removeFile(file, dispatch)
    const totalFiles = uploaded_files.length-1
    const perPage = process.env.PER_PAGE
    if (totalFiles - ((current_page - 1) * perPage) === 0) {
      setCurrentPage(current_page - 1, dispatch)
    }
  }

  const handlePageChange = pageNumber => {
    setCurrentPage(pageNumber, dispatch)
  }

  const handleFormShow = (bool) => {
    setCollapsed(bool);
    setErrMessage('');
    setFolderName('');
  }

  const handleOpenShareModal = (bool) => {
    if (!bool) {
      clearShortLink(dispatch);
    }
    setCollapsedModal(bool);
    const url = window.location.href;
    generateShortLink(`${process.env.INVOKE_URL}/url-shorten`, `${url}share/${encrpter.encrypt(breadcrumbs.join('/'))}`, dispatch);
  }

  const handleFolderChange = e => {
    setFolderName(e.target.value);
  }

  const handleFolderCreation = () => {
    setCreatingFolder(true);
    awsS3.createFolder(folderName, breadcrumbs)
      .then((data) => {
        setCreatingFolder(false);
        setCollapsed(false);
        setFolderName('');
        storeUploadedFiles(data, dispatch);
        toastr.success('Folder has been created successfully.');
      })
      .catch(err => {
        setCreatingFolder(false);
        setErrMessage(err.message);
      })
  }

  const handleListFolder = (folder) => {
    // loading.current = true;
    setLoading(true, dispatch);
    let names = breadcrumbs;
    const index = breadcrumbs.indexOf(folder);
    switch (true) {
      case folder === 'home':
        names = [user.username];
        break;
      case index === -1 && folder !== 'home':
        names.push(folder);
        break;
      case index >= 0:
        names = breadcrumbs.splice(0, (index + 1));
        break;
      default:
        break;
    }

    setBreadcrumbs(names, dispatch);
    setCurrentPage(1, dispatch);
    localStorage.setItem('breadcrumbs', JSON.stringify(names));

    awsS3.listBucket(names)
      .then(data => {
        // loading.current = false;
        setLoading(false, dispatch);
        storeUploadedFiles(data, dispatch);
      })
      .catch(err => {
        // loading.current = false;
        setLoading(false, dispatch);
        toastr.error("An error occurred!")
      })
  }

  const props = {
    collapsed,
    folderName,
    errMessage,
    activeUser,
    handleDelete,
    handleFormShow,
    creatingFolder,
    handleListFolder,
    handlePageChange,
    handleFolderChange,
    handleFolderCreation,
    files: uploaded_files,
    loading: loading.current,
    currentPage: current_page,
  };

  const breadcrumbItems = () => {
    const breadcrumbItems = local && local.length ? local : breadcrumbs;
    return breadcrumbItems.map((breadcrumb, index) => (
      <li key={index}>
        { breadcrumbItems.length - 1 !== index
          ? <a onClick={() => handleListFolder(breadcrumb)}>{ breadcrumb }</a>
          : breadcrumb }
      </li>
    ))
  }

  return (
    <div className="wrapper">
      <div className="flex-container">
        <section className="left-panel">
          <div className="uploadable-container">
            <div className={cl('form-section', { 'disappear': hideForm, 'appear': !hideForm })}>
              <p className="title">Upload file ({files.length})</p>
              <div className="form-body">
                <form encType="multipart/form-data">
                  <label htmlFor="file" className="file-label">
                    <input
                      className="box__file"
                      type="file"
                      name="files"
                      id="file"
                      ref={inputEl}
                      onChange={handleFileChange}
                      data-multiple-caption="{count} files selected"
                      multiple
                      disabled={activeUser && activeUser.role !== 'owner'}
                    />
                    {
                      inputEl.current && inputEl.current.files.length
                      ? <div className="selected-files">
                          <p className="file-count">
                            { files.length } file(s) selected
                          </p>
                          <ul className="file-list">
                            { files.map((file, index) => (
                              <li key={index}>{file.name}</li>
                            )) }
                          </ul>
                        </div>
                      : <div className="box-instruction">
                          <div className="upload-picture">
                            <i className="fas fa-cloud-upload-alt"></i>
                          </div>
                          <p>Please drag file here or click to select file(s)</p>
                          <a className="select-button">Choose</a>
                        </div>
                    }

                  </label>
                </form>
                <div className="form-control">
                  <button className="upload-btn" type="button" disabled={!inputEl.current || !inputEl.current.value || (activeUser && activeUser.role !== 'owner')} onClick={handleUpload}>Upload</button>
                </div>
              </div>
            </div>
            { hideForm && <div className={cl('progress-section', { 'fly-up': hideForm, 'fly-down': !hideForm })}>
              <div className="title">
                <div className="total">Total <span className="uploaded-number">{uploaded.length}</span> / <span className="total-number">{files.length}</span></div>
              </div>
              <div className="uploading-progress">
                <div className="progress-bar-container">
                  { showProgressBars() }
                </div>
              </div>
            </div> }
          </div>
        </section>
        <section className="main-panel">
          <nav className="sub-head">
            <div className="left-nav">
              <button
                type="button"
                onClick={() => handleFormShow(true)}
                disabled={activeUser && activeUser.role !== 'owner'}
              >
                <i className="fas fa-plus"></i>Create folder
              </button>
              <button
                className="btn-share"
                type="button"
                onClick={() => handleOpenShareModal(true)}
                disabled={activeUser && activeUser.role !== 'owner'}
              >
                <i className="fas fa-share-square"></i>Share
              </button>
            </div>
            <div className="right-nav">
              <ul className="breadcrumbs">
                <li>
                  <a onClick={() => handleListFolder('home')}>
                    <i className="fas fa-home"></i>
                  </a>
                </li>
                { breadcrumbItems() }
              </ul>
            </div>
          </nav>
          <div className='file-layout'>
            <React.Suspense fallback={<div className="loading-file-list"><i className="icon-spin5 animate-spin"></i></div>}>
                <FileList {...props}/>
            </React.Suspense>
          </div>
        </section>
        <ShareModal
          shorten_url={shorten_url}
          collapsedModal={collapsedModal}
          handleOpenShareModal={handleOpenShareModal}
        />
      </div>
    </div>
  )
}
