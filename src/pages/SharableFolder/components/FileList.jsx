import React from 'react';
import cl from 'classnames';
import Pagination from 'react-js-pagination';

export default function FileList (props) {
  const {
    files,
    loading,
    errMessage,
    currentPage,
    handlePageChange,
    handleListFolder
  } = props
  const totalFiles = files.length
  const perPage = Number(process.env.PER_PAGE);
  const start = (currentPage - 1) * perPage
  const end = (start + perPage) < files.length ? (start + perPage) : files.length;
  let items = []

  if (files.length) {
    for (let i = start; i < end; i++) {
      items = [...items, {
        type: files[i].type,
        id: files[i].id ? files[i].id : '---',
        name: files[i].name,
        size: files[i].size ? files[i].size : '---',
        url: files[i].url ? files[i].url : '---',
        author: files[i].author ? files[i].author : '---',
        key: files[i].key && files[i].key,
        last_modified: files[i].last_modified ? files[i].last_modified : '---'
      }];
    }
  }

  return (
    <div className="table-container">
      <table>
        <thead>
          <tr>
            <th width="200">Name</th>
            <th width="150">Size (MB)</th>
            <th width="400">Path</th>
            <th width="100">Last modified</th>
          </tr>
        </thead>
        <tbody>
          { items.length ? items.map((file, index) => (
            <tr key={index}>
              <td className="td-name">
                {
                  file.type === 'folder'
                  ? <a onClick={() => handleListFolder(file.name)}><i className="fas fa-folder-open"></i> {file.name}</a>
                    : <a href={file.url} title={file.name}>{ file.name }</a>
                }
              </td>
              <td className="td-size">{ file.type === 'file' ? (file.size / (1000 ** 2)).toFixed(1) : file.size}</td>
              <td className="td-path">
                {
                  file.type === 'file'
                  ? <a href={file.url} title={file.url}>{file.url}</a>
                    : <span>{file.url}</span>
                }
              </td>
              <td className="td-last-modified">{file.last_modified}</td>
            </tr>
          ))
            :
            <tr>
              <td colSpan={5} align="center">{loading ? <i className="icon-spin5 animate-spin"></i> : 'Data Not Found'} </td>
            </tr>
          }
        </tbody>
      </table>
      { items.length ? <Pagination
        firstPageText={<i className="icon-angle-double-left"></i>}
        lastPageText={<i className="icon-angle-double-right"></i>}
        activePage={currentPage}
        itemsCountPerPage={perPage}
        totalItemsCount={totalFiles}
        onChange={handlePageChange}
      /> : ''}
    </div>
  )
}