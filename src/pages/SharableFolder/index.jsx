import React, { useContext, useEffect, useRef, useState } from 'react';
import encrpter from "../../utils/encrypter";
import { Store } from "../../Store";
import './style.scss';
import {storeUploadedFiles} from "../Home/action";
import awsS3 from "../../utils/awsS3";
import Toastr from "../../utils/notifications";
const FileList = React.lazy(() => import('./components/FileList'));

const toastr = new Toastr();

export default function SharableFolder(props) {
  const loading = useRef(true);
  const { state, dispatch } = useContext(Store);

  const [root, setRoot]               = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [breadcrumbs, setBreadcrumbs] = useState([]);
  const { uploaded_files } = state;

  useEffect(() => {
    let folder = encrpter.decrypt(props.name);
    folder = folder.split('/');
    loading.current = true;

    awsS3.listBucket(folder)
      .then(data => {
        loading.current = false;
        setRoot(folder.splice(0, folder.length - 1))
        setBreadcrumbs([folder.pop()])
        storeUploadedFiles(data, dispatch);
      })
      .catch(() => {
        toastr.error('An error has occurred!');
      })
    return () => {
      storeUploadedFiles({files: [], folders: []}, dispatch);
      loading.current = true;
    }
  }, []);

  const handleListFolder = (folder) => {
    loading.current = true;
    let names = breadcrumbs;
    const index = breadcrumbs.indexOf(folder);
    switch (true) {
      case index === -1:
        names.push(folder);
        break;
      case index >= 0:
        names = breadcrumbs.splice(0, (index + 1));
        break;
      default:
        break;
    }

    setBreadcrumbs(names);
    setCurrentPage(1);

    awsS3.listBucket([...root, ...names])
      .then(data => {
        loading.current = false;
        storeUploadedFiles(data, dispatch);
      })
      .catch(err => {
        loading.current = false;
        toastr.error("An error occurred!")
      })
  }

  const handlePageChange = pageNumber => {
    setCurrentPage(pageNumber)
  }

  const subProps = {
    handleListFolder,
    handlePageChange,
    currentPage,
    files: uploaded_files,
    loading: loading.current
  };

  const breadcrumbItems = () => {
    return breadcrumbs.map((breadcrumb, index) => (
      <li key={index}>
        { breadcrumbs.length === 1
          ? <i className="fas fa-home"></i>
          : (
            breadcrumbs.length - 1 !== index
              ? <a onClick={() => handleListFolder(breadcrumb)}>{ index === 0 ? <i className="fas fa-home"></i> : breadcrumb }</a>
              : breadcrumb
          )
        }
      </li>
    ))
  }

  return (
    <div className="share-wrapper">
      <div className="flex-container">
        <div className="navigation">
          <ul className="breadcrumbs">
            { breadcrumbItems() }
          </ul>
        </div>
        <React.Suspense fallback={<div className="loading-file-list"><i className="icon-spin5 animate-spin"></i></div>}>
          <FileList {...subProps}/>
        </React.Suspense>
      </div>
    </div>
  )
}