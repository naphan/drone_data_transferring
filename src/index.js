import React, {useEffect} from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Public from './Public';
import { Router } from '@reach/router';
import LoginPage from './pages/Login';
import HomePage from './pages/Home';
import SharableFolder from './pages/SharableFolder';
import { StoreProvider } from './Store';
import './assets/sass/main.scss';

ReactDOM.render(
  <StoreProvider>
    <Router>
      <App path='/'>
        <HomePage path='/' />
        <LoginPage path='/login' />
      </App>
      <Public path="/share">
        <SharableFolder path="/:name" />
      </Public>
    </Router>
  </StoreProvider>,
  document.getElementById('root')
);