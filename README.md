Terra Inspection Source Code Structure
============================

> Base folder structure and meaning

### A typical top-level directory layout

    .
    ├── README.md
    ├── package.json
    ├── public
    |   ├── index.html
    |   ├── favicon.ico
    ├── src
    │   ├── assets
    │   ├── index.js
    │   ├── layouts
    │   ├── pages
    |   |   ├── Home
    |   |   |   ├── private.jsx
    |   |   ├── Login
    |   |   |   ├── private.jsx
    │   ├── App.jsx
    │   ├── index.js
    │   ├── Store.js
    ├── .babelrc
    ├── .package.json
    ├── webpack.config.js

`src/assets`

Fonts, images, style...


`public/index.html`

Template for webpack running build.

`src/layouts`

Layouts: Main Layout, Auth Layout,...

`src/reducers`

Root reducers.

`src/routes`

Project routes.

`src/sagas`

Root sagas, API Service for request to outside.

`src/store`

Root store.

`src/styles`

Themes, Scss,...

`src/translations`

Define messages for multilanguage.

`src/utils`

Common helpers: Reducer injector, Saga injector, Constants,...

`webpack`

Webpack configuration.
=======
# drone_data_transferring

