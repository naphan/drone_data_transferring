const path = require('path');
const dotenv = require('dotenv');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const devMode = process.argv[process.argv.indexOf('--mode') + 1] !== 'production';

const config = {
  entry: './src/index.js',
  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          // 'postcss-loader',
          'sass-loader',
        ],
      },
      // {
      //   test: /\.scss$/,
      //   use: [
      //     "style-loader", // creates style nodes from JS strings
      //     "css-loader", // translates CSS into CommonJS
      //     "sass-loader" // compiles Sass to CSS, using Node Sass by default
      //   ]
      // },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {},
          },
        ],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
        use: ['file-loader']
      },
      {
        type: 'javascript/auto',
        test: /\.json$/,
        use: ['json-loader']
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.json']
  },
  output: {
    path: __dirname + '/dist',
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'public/index.html'),
      favicon: path.join(__dirname, 'public/favicon.ico'),
    }),
    // new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    historyApiFallback: true,
    contentBase: './dist'
  }
};

module.exports = (env, argv) => {
  const environment = dotenv.config().parsed
  const envKeys = Object.keys(environment).reduce((prev, next) => {
    prev[`process.env.${next}`] = JSON.stringify(environment[next])
    return prev
  }, {})

  config.plugins.push( new webpack.DefinePlugin(envKeys) );

  if (argv.mode === 'production') {
    console.log(`Production Build Started!`)

    config.plugins.push(
      new MiniCssExtractPlugin({
        filename: "css/[name].[hash].css",
        chunkFilename: "[id].[hash].css"
      })
    )

    config.output.filename = 'js/[name].[hash].js'
  }

  return config;
}